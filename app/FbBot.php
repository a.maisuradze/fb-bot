<?php

use pimax\FbBotApp;
use pimax\Messages\Message;
use pimax\Messages\ImageMessage;

class FbBot
{
    /**
     * Путь лог-файла
     */
    const LOG_PATH = 'app.log';

    /**
     * @var FbBotApp
     */
    protected $app;

    /**
     * Настройки
     *
     * @var array
     */
    protected $config;

    /**
     * Входящие данные от fb
     *
     * @var array
     */
    protected $data;


    /**
     * fbot constructor.
     * @param FbBotApp $app
     * @param array $config
     */
    public function __construct(FbBotApp $app, array $config)
    {
        $this->app = $app;
        $this->config = $config;
    }

    public function start()
    {
        FbBot::log("Server bot started.");

        //Подписка
        if (!empty($_REQUEST['hub_mode']) && $_REQUEST['hub_mode'] == 'subscribe' && $this->verifyToken($_REQUEST['hub_verify_token'])) {
            FbBot::log("Subscribe.");
            $this->subscribe();
        }

        $this->getData();

        if (!empty($this->data['entry'][0]['messaging'])) {
            foreach ($this->data['entry'][0]['messaging'] as $message) {

                $payload = $this->getPostbackPayload($message);

                // Кнопка начать
                if ($payload == 'GET_STARTED') {
                    FbBot::log("GET_STARTED");
                    $this->getStarted($message);
                    continue;
                }

                //Пропускаем не нужное
                if ($this->isSkipping($message)) {
                    FbBot::log("skip");
                    continue;
                }

                // Сообщение
                if (!empty($message['message'])) {
                    FbBot::log("reply");
                    $this->reply($message);
                    continue;
                }

            }
        }
    }

    protected function reply($message)
    {
        $command = trim($message['message']['text']);
        FbBot::log("command: ");
        FbBot::log($command);
        if (preg_match('/^[0-9]+$/', $command)) {
            $text = "Код принят: " . $command;
            $this->app->send(new Message($message['sender']['id'], $text));
        }

        $this->app->send(new Message($message['sender']['id'], "Вы отправили ".$command));
    }

    protected function isSkipping($message)
    {
        if (
            !empty($message['delivery'])
            || $message['message']['is_echo'] == "true" // My own messages
        ) {
            return true;
        } else {
            return false;
        }
    }

    protected function getStarted($message)
    {
        $this->app->send(new Message($message['sender']['id'], "Введите код"));
    }

    protected function getPostbackPayload(array $message)
    {
        if (!empty($message['postback'])) {
            return $payload = trim($message['postback']['payload']);
        }
    }

    protected function subscribe()
    {
        echo $_REQUEST['hub_challenge'];
    }

    protected function verifyToken($token)
    {
        return ($token == $this->config['verify_token']);
    }

    protected function getData()
    {
        if (empty($this->data))
            $this->data = json_decode(file_get_contents("php://input"), true, 512, JSON_BIGINT_AS_STRING);
        return $this->data;
    }


    /**
     * Logger
     * @param mixed $data
     */
    public static function log($data)
    {
        file_put_contents(__DIR__ . '/' . self::LOG_PATH, date('Y-m-d H:m:i') . ": " . print_r($data, 1) . "\n", FILE_APPEND);
    }

    public function sendMessage($recipient)
    {
        $message = new ImageMessage($recipient, dirname(__FILE__) . '/1.jpeg');

        $message_data = $message->getData();
        $message_data['message']['attachment']['payload']['url'] = '1.jpeg';

        echo '<pre>', print_r($message->getData()), '</pre>';

        $res = $this->app->send($message);

        echo '<pre>', print_r($res), '</pre>';
    }

}