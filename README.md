## Facebook Bot

Project structure:
```
├── docker-compose.yaml
├── app
    ├── Dockerfile
    └── config.php
    └── index.php
    └── send.php

```

## First start

```
$ docker-compose up -d
$ docker-compose exec -T php php composer.phar install
```

## Start
```
$ docker-compose up -d
```

## Stop
```
$ docker-compose down
```
